<?php

namespace App\Controller;

use App\Entity\FilterPost;
use App\Entity\Post;
use App\Entity\User;
use App\Form\FilterType;
use App\Form\PostType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PostController extends AbstractController
{
    /**
     * @Route("/newpost/{id}", name="newpost")
     */
    public function newpost(Request $request, EntityManagerInterface $em,int $id)
    {
        $Post = new Post();

        $repository = $em->getRepository(User::class);
        $user = $repository->findOneBy(array('id'=> $id));

        $form = $this->createFormBuilder($Post)
                     ->add('name')
                     ->add('description')
                     ->add('categorie', ChoiceType::class,[
                         'choices'=>[
                             'jeu de plateform'=>'jeu de plateform',
                             'jeu de strategie'=>'jeu de strategie',
                         ]
                     ])
                     ->add('lien')
                     ->add('image')
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $Post->setNbTelechargement(0);
            $Post->setUser($user);

            $em->persist($Post);
            $em->flush();

            return $this->redirectToRoute('homepage');

        }

        return $this->render('post/newpost.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/listpost", name="listpost")
     */
    public function listpost(Request $request, EntityManagerInterface $em)
    {
        $filterPost = new FilterPost();

        $form = $this->createFormBuilder($filterPost)
                     ->add('name')
                     ->add('categorie', ChoiceType::class,[
                        'choices'=>[
                        'toutes les categorie'=>'toutes les categorie',
                        'jeu de plateform'=>'jeu de plateform',
                        'jeu de strategie'=>'jeu de strategie',
                     ]
                     ])
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $repository = $em->getRepository(Post::class);


            $Posts = $repository->findallpostbyFilter($filterPost);

            return $this->render('post/listpost.html.twig', [
                'form' => $form->createView(),
                'Posts' => $Posts
            ]);

        }
        
        $repository = $em->getRepository(Post::class);

        $Posts = $repository->findallpostbyFilter($filterPost);

        return $this->render('post/listpost.html.twig',[
            'form'=>$form->createView(),
            'Posts' => $Posts
            ]);
    }

    /**
     * @Route("/editpost/{id}", name="editpost")
     */
    public function editpost(Request $request, EntityManagerInterface $em,int $id)
    {
        $repository = $em->getRepository(Post::class);

        $Post = $repository->findOneBy(array('id'=> $id));

        $form = $this->createFormBuilder($Post)
                     ->add('name')
                     ->add('description')
                     ->add('categorie', ChoiceType::class,[
                        'choices'=>[
                            'jeu de plateform'=>'jeu de plateform',
                            'jeu de strategie'=>'jeu de strategie',
                        ]
                    ])
                     ->add('image')
                     ->add('lien')
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em->persist($Post);
            $em->flush();

            return $this->redirectToRoute('listpost');

        }

        return $this->render('post/editpost.html.twig',[
            'form' => $form->createView()
            ]);
    }

    /**
     * @Route("/add1telechargement/{id}/{idu}", name="add1telechargement")
     */
    public function add1telechargement(Request $request, EntityManagerInterface $em,int $id,int $idu){
        $repository = $em->getRepository(Post::class);
        $repository2 = $em->getRepository(User::class);

        $Post = $repository->findOneBy(array('id'=> $id));
        $User = $repository2->findOneBy(array('id'=>$idu));

        $users = $Post->getUsers();

        $url = $Post->getLien();


        for ($i = 0; $i < count($users); ++$i) {
            if ($users[$i]->getId() == $User->getId()) {
                
                return $this->redirect($url);    
            }
        }
        $Post->addUser($User);
        $Post->setNbTelechargement($Post->getNbTelechargement() + 1);

        $em->persist($Post);
        $em->flush();

        
        return $this->redirect($url);
        
    }

    /**
     * @Route("/deletpost/{id}", name="deletpost")
     */
    public function deletpost(Request $request, EntityManagerInterface $em,int $id){
        $repository = $em->getRepository(Post::class);

        $Post = $repository->findOneBy(array('id'=> $id));


        $em->remove($Post);
        $em->flush();

        return $this->redirectToRoute('listpost');
        
    }
}
