<?php

namespace App\Controller; 

use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController
{
    
   /**
    * @Route("/", name="homepage")
    **/
    public function hompage(Request $request, EntityManagerInterface $em) {
        $repository = $em->getRepository(Post::class);

        $Posts = $repository->find10lastpost();


        return $this->render('index.html.twig', [
            'title' => 'les 10 dernier jeux',
            'Posts'=>$Posts
        ]);
        
    }
}