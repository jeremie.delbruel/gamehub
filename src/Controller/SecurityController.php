<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('security_login');

        }

        return $this->render('security/registration.html.twig',[
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/connexion", name="security_login")
     */
    public function login(){
        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout(){
        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/myaccount", name="security_myacount")
     */
    public function account(){
        return $this->render('security/account.html.twig');
    }

    /**
     * @Route("/editaccount/{pseudo}", name="security_editacount")
     */
    public function editaccount(Request $request, EntityManagerInterface $em,string $pseudo){
        $repository = $em->getRepository(User::class);
        $user = $repository->findOneBy(array('pseudo'=> $pseudo));
        
        $form = $this->createFormBuilder($user)
                     ->add('name')
                     ->add('surname')
                     ->add('age')
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('security_myacount');

        }

        return $this->render('security/editaccount.html.twig',[
            'form' => $form->createView()
        ]);
    }

}
