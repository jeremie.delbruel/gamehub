<?php

namespace App\Repository;

use App\Entity\FilterPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FilterPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilterPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilterPost[]    findAll()
 * @method FilterPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilterPostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FilterPost::class);
    }

    // /**
    //  * @return FilterPost[] Returns an array of FilterPost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FilterPost
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    
}
