<?php

namespace App\Repository;

use App\Entity\FilterPost;
use App\Entity\Post;
use App\Form\FilterType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findallpostbyFilter(FilterPost $filter)
    {
        if (empty($filter->getName())) {
            if ($filter->getCategorie() == "toutes les categorie") {
                return $this->createQueryBuilder('p')
                            ->orderBy('p.id', 'DESC')
                            ->getQuery()
                            ->getResult();
            } else {
                return $this->createQueryBuilder('p')
                            ->andWhere('p.categorie = :categorie')
                            ->setParameter('categorie', $filter->getCategorie())
                            ->orderBy('p.id', 'DESC')
                            ->getQuery()
                            ->getResult();
            }
        } else {
            if ($filter->getCategorie() == "toutes les categorie") {
                return $this->createQueryBuilder('p')
                            ->andWhere('p.name LIKE :name')
                            ->setParameter('name', "%{$filter->getName()}%")
                            ->orderBy('p.id', 'DESC')
                            ->getQuery()
                            ->getResult();
            }
            return $this->createQueryBuilder('p')
                        ->andWhere('p.categorie = :categorie')
                        ->setParameter('categorie', $filter->getCategorie())
                        ->andWhere('p.name LIKE :name')
                        ->setParameter('name', "%{$filter->getName()}%")
                        ->orderBy('p.id', 'DESC')
                        ->getQuery()
                        ->getResult();
        }
    }

    public function find10lastpost()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->setMaxResults(10)
            ->getResult(); 
    }
}
